# PE P22 : Développement d’IAs pour les échecs

## Présentation du projet : 
    Les intelligences artificielles (IA) sont de plus en plus sophistiquées de nos jours. Elles sont utilisées dans des contextes variés :
    automatisation, résolution de problèmes, analyse de données, reconnaissance automatique…
    Passionné de développement de jeux, j’ai déjà créé quelques IA de base (pathfinding, prise de décision simple…) et aimerai me
    lancer dans une IA plus complexe, en partant d’un problème connu. Après quelques recherches, le jeu d’échecs m’a paru
    approprié puisqu’assez complexe pour voir de nombreuses notions et assez connu pour être bien documenté (on peut citer par
    exemple https://www.chessprogramming.org).
    L’objectif de ce projet est de développer une intelligence artificielle pour le jeu d’échecs et de l’améliorer au maximum, autant dans
    sa rapidité de calcul que dans la qualité de ses décisions. Cette IA se basera sur un arbre de décision et sera améliorée via
    l’apprentissage automatique.

## Acteurs
### Tuteur PE : Joris Falip
### Etudiant : Thibaud Macret

## Avancement du projet
    L'avancement du projet peut être suivi via la catgorie issues sur GitLab.

## Ressources
    https://www.chessprogramming.org/Minimax

## Instalation et utilisation
    Executable : chess.exe
