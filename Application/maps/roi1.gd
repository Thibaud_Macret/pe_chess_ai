extends Node2D

#Fomat d'une map : 
#une variable par morceau de partie
#chaque variable contient le tableau blanc en [0] et le noir en [1]
#une map est un tableau 64*1, comme le plateau de jeu
var debut:Array = [[
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-8,-8,-9,-9,-9,
-9,-9,-8,-7,-7,-8,-9,-9,
-9,-8,-7,-6,-6,-7,-8,-9,
00,05,10,02,03,02,10,00
],[
00,05,10,02,03,02,10,00,
-9,-8,-7,-6,-6,-7,-8,-9,
-9,-9,-8,-7,-7,-8,-9,-9,
-9,-9,-9,-8,-8,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9,
-9,-9,-9,-9,-9,-9,-9,-9
]]

var millieu:Array = [[
-6,-6,-6,-6,-6,-6,-6,-6,
-5,-5,-5,-5,-5,-5,-5,-5,
-4,-4,-4,-4,-4,-4,-4,-4,
-3,-3,-3,-3,-3,-3,-3,-3,
-2,-2,-2,-2,-2,-2,-2,-2,
-1,-1,-1,-1,-1,-1,-1,-1,
00,00,00,00,00,00,00,00,
00,05,10,02,03,02,10,00
],[
00,05,10,02,03,02,10,00,
00,00,00,00,00,00,00,00,
-1,-1,-1,-1,-1,-1,-1,-1,
-2,-2,-2,-2,-2,-2,-2,-2,
-3,-3,-3,-3,-3,-3,-3,-3,
-4,-4,-4,-4,-4,-4,-4,-4,
-5,-5,-5,-5,-5,-5,-5,-5,
-6,-6,-6,-6,-6,-6,-6,-6
]]

var fin:Array = [[
00,01,02,03,03,02,01,00,
01,02,03,04,04,03,02,01,
02,03,04,05,05,04,03,02,
03,04,05,06,06,05,04,03,
03,04,05,06,06,05,04,03,
02,03,04,05,05,04,03,02,
01,02,03,04,04,03,02,01,
00,01,02,03,03,02,01,00
],[
00,01,02,03,03,02,01,00,
01,02,03,04,04,03,02,01,
02,03,04,05,05,04,03,02,
03,04,05,06,06,05,04,03,
03,04,05,06,06,05,04,03,
02,03,04,05,05,04,03,02,
01,02,03,04,04,03,02,01,
00,01,02,03,03,02,01,00
]]
