extends Node2D

var colors:Array = [
Color(1,0,0,0.1),
Color(0.9,0,0,0.9),
Color(0.8,0,0,0.8),
Color(0.7,0,0,0.7),
Color(0.6,0,0,0.6),
Color(0.5,0,0,0.5),
Color(0.4,0,0,0.4),
Color(0.3,0,0,0.3),
Color(0.2,0,0,0.2),
Color(0.1,0,0,0.1),
Color(0,0,0,0),
Color(0,0,0.1,0.1),
Color(0,0,0.2,0.2),
Color(0,0,0.3,0.3),
Color(0,0,0.4,0.4),
Color(0,0,0.5,0.5),
Color(0,0,0.6,0.6),
Color(0,0,0.7,0.7),
Color(0,0,0.8,0.8),
Color(0,0,0.9,0.9),
Color(0,0,1,1)
]

func _ready():
	visu_debut()

func visu_debut():
	var cases = $Map.fin
	for case in range(64):
		$Grille.get_child(case).color = colors[10 + cases[0][case]]
