extends Node2D

var pieceActive:Piece

func _ready():
	globals.refVisu = self
	$HUD/lblJB.text = globals.refModele.joueurBlanc.strategie.toString()
	$HUD/lblJN.text = globals.refModele.joueurNoir.strategie.toString()

func reset_board():
	pieceActive = null
	for case in $Board.get_children():
		case.reset_couleur()
	if(globals.refModele.est_en_echec(globals.refModele.joueurActif)):
		$Board.get_child($Pieces.get_node("RoiB" if globals.refModele.joueurActif else "RoiN").case).active_case_echec()

func actualise_board():
	reset_board()
	var nbPiecesPrises:Vector2 = Vector2(0,0)
	for numPiece in range (32):
		var pieceVisu = $Pieces.get_child(numPiece)
		var piece = globals.refModele.pieces[0 if pieceVisu.joueurB else 1][pieceVisu.indexModele]
		pieceVisu.case = piece.case
		if(piece.case == -1):
			pieceVisu.position = Vector2(-700 if piece.joueurB else -600, -365 + (80*nbPiecesPrises[0 if piece.joueurB else 1]))
			nbPiecesPrises[0 if piece.joueurB else 1] +=1
		else :
			pieceVisu.position = $Board.get_child(piece.case).position + Vector2(48,48)
		if(pieceVisu.piece != piece.piece):
			pieceVisu.piece = piece.piece
			pieceVisu.actualise_image()
	$HUD/lblJB.add_color_override("font_color", Color(0,0,0) if !globals.refModele.joueurActif else Color(255,255,255))
	$HUD/lblJN.add_color_override("font_color", Color(0,0,0) if globals.refModele.joueurActif else Color(255,255,255))


func on_piece_click(piece:Piece):
	if(piece.case != -1 && piece.joueurB == globals.refModele.joueurActif):
		if(piece == pieceActive): #déselection
			reset_board()
			return
		reset_board()
		pieceActive = piece
		$Board.get_child(piece.case).active_case_choix()
		for case in globals.refModele.calcule_possibilitees_piece(piece.case):
			$Board.get_child(case).active_case()

# warning-ignore:unused_argument
func on_case_click(case:Case):
	globals.refModele.bouge_piece([globals.refModele.cases[pieceActive.case],case.id], (1 if globals.refModele.joueurActif else 2))
# warning-ignore:integer_division
# warning-ignore:integer_division
	if(pieceActive.piece != 5 || !(case.id/8 == 0 || case.id/8 == 7)): #Si pas de promo, on passe le tour
		globals.refModele.passe_tour()

func on_promotion_click(piece:int):
	$BoutonsPromotion.visible = false
	globals.refModele.promotionPiece = piece
