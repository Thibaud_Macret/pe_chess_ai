extends Node

var pieces:Array = [[],[]] #[B][N]
var cases:Array
var joueurActif:bool = false #false car on change le tour pour lancer
var joueurBlanc:Joueur = load("res://scripts/Joueur.gd").new()
var joueurNoir:Joueur = load("res://scripts/Joueur.gd").new()
var visualisation:bool = true
var timer:Timer #Timer pour mettre un micro délai entre deux tours
var precedentesPositions:Array = ["A1","B1","C1","D1","A2","B2","C2","D2"] #Pour tester les pats

func _ready():
# warning-ignore:unused_variable
	for indexCase in range (65): #65 pour éviter le bug de la case -1
		cases.push_back(0)
	init_timer()
	init_pieces()
	init_joueurs()
	actualise_cases()
	globals.refModele = self
	passe_tour()

func init_timer():
	timer = Timer.new()
	timer.set_wait_time(.001)
	timer.set_one_shot(true)
	self.add_child(timer)

func init_pieces():
	var pieceScript = load("res://scripts/PieceModele.gd")
# warning-ignore:unused_variable
	for indexPiece in range (16):
		pieces[0].push_back(pieceScript.new())
		pieces[1].push_back(pieceScript.new())
	
	pieces[0][0].init(5,true,48)
	pieces[0][1].init(5,true,49)
	pieces[0][2].init(5,true,50)
	pieces[0][3].init(5,true,51)
	pieces[0][4].init(5,true,52)
	pieces[0][5].init(5,true,53)
	pieces[0][6].init(5,true,54)
	pieces[0][7].init(5,true,55)
	pieces[0][8].init(2,true,56)
	pieces[0][9].init(4,true,57)
	pieces[0][10].init(3,true,58)
	pieces[0][11].init(1,true,59)
	pieces[0][12].init(3,true,61)
	pieces[0][13].init(4,true,62)
	pieces[0][14].init(2,true,63)
	pieces[0][15].init(0,true,60)
	
	pieces[1][0].init(2,false,0)
	pieces[1][1].init(4,false,1)
	pieces[1][2].init(3,false,2)
	pieces[1][3].init(1,false,3)
	pieces[1][4].init(3,false,5)
	pieces[1][5].init(4,false,6)
	pieces[1][6].init(2,false,7)
	pieces[1][7].init(5,false,8)
	pieces[1][8].init(5,false,9)
	pieces[1][9].init(5,false,10)
	pieces[1][10].init(5,false,11)
	pieces[1][11].init(5,false,12)
	pieces[1][12].init(5,false,13)
	pieces[1][13].init(5,false,14)
	pieces[1][14].init(5,false,15)
	pieces[1][15].init(0,false,4)

func init_joueurs():
	match globals.joueurs[0]:
		1:
			joueurBlanc.init(true, load("res://scripts/strategies/StrategieHumain.gd").new(), true)
		2:
			joueurBlanc.init(true, load("res://scripts/strategies/StrategieIaBase.gd").new(), false)
		3:
			joueurBlanc.init(true, load("res://scripts/strategies/StrategieIaAvancee.gd").new(), false)
	match globals.joueurs[1]:
		1:
			joueurNoir.init(false, load("res://scripts/strategies/StrategieHumain.gd").new(), true)
		2:
			joueurNoir.init(false, load("res://scripts/strategies/StrategieIaBase.gd").new(), false)
		3:
			joueurNoir.init(false, load("res://scripts/strategies/StrategieIaAvancee.gd").new(), false)

func actualise_cases():
	for case in 64:
		cases[case] = 0
	for piece in pieces[0]:
		cases[piece.case] = piece
	for piece in pieces[1]:
		cases[piece.case] = piece

func get_joueur_actif() -> Joueur:
	return(joueurBlanc if joueurActif else joueurNoir)

func conversion_string() -> String:
	var bufferedString:String = ""
	for piece in pieces[0]:
		if (piece.case>-1 && piece.case<10):
			bufferedString +="0"
		bufferedString += String(piece.case)
	for piece in pieces[1]:
		if (piece.case>-1 && piece.case<10):
			bufferedString +="0"
		bufferedString += String(piece.case)
	for indexPion in range (8):
		bufferedString += String(pieces[0][indexPion].piece)
		bufferedString += String(pieces[1][indexPion+7].piece)
	return bufferedString

func recuperation_string(source:String):
	for indexPiece in range (16):
		pieces[0][indexPiece].case = (int(source[indexPiece*2])*10 + int(source[indexPiece*2+1]) if(source[indexPiece*2] != "-") else -1)
		pieces[1][indexPiece].case = (int(source[indexPiece*2+32])*10 + int(source[indexPiece*2+1+32]) if(source[indexPiece*2+32] != "-") else -1)
	for indexPion in range (8):
		pieces[0][indexPion].piece = int(source[64 + indexPion*2])
		pieces[1][indexPion+7].piece = int(source[64 + indexPion*2 + 1])
	actualise_cases()

#Coup = [piece:PieceModele, case:int]
#promoType corresponds à la manière de promouvoir : 0 pour une promo par défaut (dame) 1 ou 2 pour la promo d'un joueur
func bouge_piece(coup:Array, promoType:int=0):
	if(typeof(cases[coup[1]]) != TYPE_INT): #Si pièce sur la case, on prends
		cases[coup[1]].case = -1
	roque(coup)
	en_passant(coup)
	promotion(coup, promoType)
	cases[coup[0].case] = 0
	cases[coup[1]] = coup[0] #On change la pièce sur la case
	coup[0].case = coup[1] #On change la case de notre pièce

func roque(coup:Array):
	if(coup[0].piece == 0 && abs(coup[0].case - coup[1]) == 2):
		match (coup[1]):
			2:
				bouge_piece([pieces[1][0], 3])
			6:
				bouge_piece([pieces[1][6], 5])
			58:
				bouge_piece([pieces[0][8], 59])
			62:
				bouge_piece([pieces[0][14], 61])

func en_passant(coup:Array):
	if(coup[0].piece == 5):
		if(coup[1] == coup[0].case + (-16 if coup[0].joueurB else 16)):
			coup[0].estEnPassantAble = true #Marque le fait d'être prennable
		var caseEnPassant = cases[coup[1] + (8 if coup[0].joueurB else -8)]
		if(est_en_prise(coup) && typeof(caseEnPassant) != TYPE_INT && caseEnPassant.piece == 5 && caseEnPassant.joueurB != coup[0].joueurB): #Vérifie la prise
			caseEnPassant.case = -1 #Prend en passant

func est_en_prise(coup:Array) -> bool :
	if(coup[1] == coup[0].case + (-8 if coup[0].joueurB else 8)):
		return false
	if(coup[1] == coup[0].case + (-16 if coup[0].joueurB else 16)):
		return false
	return true

var promotionPiece:int = -1
func promotion(coup:Array, promoType:int): #TOFIX
	if(coup[0].piece == 5 && coup[1]/8 == (0 if coup[0].joueurB else 7)):
		promotionPiece = (1 if promoType == 0 else (joueurBlanc if promoType == 1 else joueurNoir).promotion())
		if(promotionPiece == -1):
			while(promotionPiece == -1):
				$"../BoutonsPromotion".visible = true
				timer.start()
				yield(timer, "timeout")
			coup[0].piece = promotionPiece
			passe_tour()
		else :
			coup[0].piece = promotionPiece
		promotionPiece = -1

func passe_tour() :
	joueurActif = !joueurActif
	if(get_joueur_actif().peutRoque):
		if(est_en_echec(joueurActif) || pieces[0][15].case != 60 if joueurActif else pieces[1][15].case != 4):
			get_joueur_actif().peutRoque = false
	if(verifie_mat_pat(calcule_possibilitees(joueurActif, true))):
		if(visualisation):
			$"../btnQuitter".visible = true
			globals.refVisu.actualise_board()
		return

	if(visualisation):
		if(globals.refVisu!=null) :
			globals.refVisu.actualise_board()
		timer.start()
		yield(timer, "timeout")
		globals.visuActive = false
	else :
		afficher_plateau()

	demarque_en_passant()
	get_joueur_actif().jouer()

func afficher_plateau():
	var bufferedString:String = ""
	var index:int = 0
	for case in cases:
		if(typeof(case) != TYPE_INT):
			bufferedString += case.toString
		else :
			bufferedString += ("  ")
		bufferedString += ("|")
		index+=1
		if(index==8):
			print(bufferedString)
			print("------------------------")
			bufferedString = ""
			index = 0

func verifie_mat_pat(possibilitees:Array) ->int:
	if(possibilitees.size()==0):
		if(est_en_echec(joueurActif)):
			print("Echec et mat pour les" + (" blancs." if !joueurActif else " noirs."))
			return(2)
		else :
			print("Echec et pat")
			return(1)
	precedentesPositions.append(conversion_string())
	precedentesPositions.pop_front()
	if(precedentesPositions[0] == precedentesPositions[4]):
		if(precedentesPositions[1] == precedentesPositions[5]):
			print("Echec et pat")
			return(1)
	return (0)

func est_en_echec(joueurB:bool) -> bool:
	var positionRoi:int = pieces[0 if joueurB else 1][15].case
	for coupOpo in calcule_possibilitees(!joueurB, false):
		if coupOpo[1] == positionRoi:
			return true
	return false

func demarque_en_passant(joueur:bool = joueurActif):
	for piece in pieces[0 if joueur else 1]:
		piece.estEnPassantAble = false

func prochaines_possibilitees(position:String) -> Array:
	var positions:Array = []
	for coup in calcule_possibilitees(joueurActif, true):
		recuperation_string(position)
		bouge_piece(coup)
		positions.append(conversion_string())
	return positions

func calcule_possibilitees(joueurB:bool, considereEchecs:bool) -> Array:
	var possibilitees:Array = []
	var positionInitiale:String = conversion_string()
	for piece in pieces[0 if joueurB else 1]:
		if (considereEchecs) :
			for coup in case_possibles(piece):
				bouge_piece(coup)
				if(!est_en_echec(joueurB)):
					possibilitees.push_back(coup)
				recuperation_string(positionInitiale)
		else :
			possibilitees += case_possibles(piece)
	return possibilitees

func calcule_possibilitees_piece(case:int) -> Array:
	var casesPossibles:Array = []
	var joueur:bool = cases[case].joueurB
	for coupPossible in case_possibles(cases[case]):
		var positionInitiale = conversion_string()
		bouge_piece(coupPossible)
		if(!est_en_echec(joueur)):
			casesPossibles.push_back(coupPossible[1])
		recuperation_string(positionInitiale)
		demarque_en_passant(joueur) #Demarque ses propres pièces
	return casesPossibles

func case_possibles(piece:PieceModele) -> Array:
	if(piece.case!=-1):
		match(piece.piece):
			5:
				return case_possibles_pion(piece)
			4:
				return case_possibles_cavalier(piece)
			3:
				return case_possibles_fou(piece)
			2:
				return case_possibles_tour(piece)
			1:
				return case_possibles_dame(piece)
			0:
				return case_possibles_roi(piece)
	return []

func get_proprietaire_piece_case(case:int):
	if(typeof(cases[case]) != TYPE_INT):
		return cases[case].joueurB
	return null

func case_possibles_roi(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	var directionsPossibles:Array = []
	if(piece.case%8 != 0):
		directionsPossibles.append(-1)
		if(piece.case > 7):
			directionsPossibles.append(-9)
		if(piece.case < 56):
			directionsPossibles.append(7)
	if(piece.case%8 != 7):
		directionsPossibles.append(1)
		if(piece.case > 7):
			directionsPossibles.append(-7)
		if(piece.case < 56):
			directionsPossibles.append(9)
	directionsPossibles.append(-8)
	directionsPossibles.append(8)
	for direction in directionsPossibles:
		var nouvelleCase = piece.case + direction
		if(nouvelleCase > -1 && nouvelleCase < 64):
			if(get_proprietaire_piece_case(nouvelleCase)!=piece.joueurB):
				casesPossibles.append([piece,nouvelleCase])
	if(grand_roque(piece)):
		casesPossibles.append([piece, 58 if piece.joueurB else 2])
	if(petit_roque(piece)):
		casesPossibles.append([piece, 62 if piece.joueurB else 6])
	return casesPossibles

func grand_roque(piece:PieceModele) ->bool:
	if(!get_joueur_actif().peutRoque):
		return false
	if(typeof(cases[(57 if piece.joueurB else 1)]) != TYPE_INT): #Si l'une des cases est occupée
		return false
	if(typeof(cases[(58 if piece.joueurB else 2)]) != TYPE_INT):
		return false
	if(typeof(cases[(59 if piece.joueurB else 3)]) != TYPE_INT):
		return false
	for pieceTestee in pieces[1] if joueurActif else pieces[0]: #On regarde si une pièce ennemie non-roi bloque la roque
		if(pieceTestee.piece != 0) : #Le roi ne peut pas déroquer (évite de boucler)
			for case in case_possibles(pieceTestee):
				if(case[1] in ([57,58,59] if joueurActif else [1,2,3])):
					return false
	return true

func petit_roque(piece:PieceModele) ->bool:
	if(!get_joueur_actif().peutRoque):
		return false
	if(typeof(cases[(61 if piece.joueurB else 5)]) != TYPE_INT): #Si l'une des cases est occupée
		return false
	if(typeof(cases[(62 if piece.joueurB else 6)]) != TYPE_INT):
		return false
	for pieceTestee in pieces[1] if joueurActif else pieces[0]: #On regarde si une pièce ennemie non-roi bloque la roque
		if(pieceTestee.piece != 0) : #Le roi ne peut pas déroquer (évite de boucler)
			for case in case_possibles(pieceTestee):
				if(case[1] in ([61,62] if joueurActif else [5,6])):
					return false
	return true

func case_possibles_dame(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	casesPossibles += case_possibles_tour(piece)
	casesPossibles += case_possibles_fou(piece)
	return casesPossibles

func case_possibles_tour(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	var directions:Array = []
	if(piece.case%8 != 0):
		directions.append(-1)
	if(piece.case%8 != 7):
		directions.append(1)
	if(piece.case > 7):
		directions.append(-8)
	if(piece.case < 56):
		directions.append(8)

	for direction in directions:
		var dist:int = 1
		var collision:bool = false
		while(!collision):
			var nouvelleCase:int = piece.case + direction*dist
			if(get_proprietaire_piece_case(nouvelleCase)!=piece.joueurB):
				casesPossibles.append([piece,nouvelleCase])
			collision = (collisions_bords_tour(nouvelleCase, direction) || get_proprietaire_piece_case(nouvelleCase)!=null)
			dist+=1
	return casesPossibles

func collisions_bords_tour(case:int, dir:int):
	match dir:
		-1:
			return (case%8 == 0)
		1:
			return (case%8 == 7)
		-8:
			return(case<8)
		8:
			return(case>55)

func case_possibles_fou(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	var directions:Array = []
	if(piece.case%8 != 0):
		if(piece.case > 7):
			directions.append(-9)
		if(piece.case < 56):
			directions.append(7)
	if(piece.case%8 != 7):
		if(piece.case > 7):
			directions.append(-7)
		if(piece.case < 56):
			directions.append(9)

	for direction in directions:
		var dist:int = 1
		var collision:bool = false
		while(!collision):
			var nouvelleCase:int = piece.case + direction*dist
			if(get_proprietaire_piece_case(nouvelleCase)!=piece.joueurB):
				casesPossibles.append([piece,nouvelleCase])
			collision = (collisions_bords_fou(nouvelleCase, direction)  || get_proprietaire_piece_case(nouvelleCase)!=null)
			dist+=1
	return casesPossibles

func collisions_bords_fou(case:int, dir:int):
	match dir:
		7:
			return (case%8 == 0 || case>55)
		-9:
			return (case%8 == 0 || case<8)
		-7:
			return (case%8 == 7 || case<8)
		9:
			return (case%8 == 7 || case>55)

func case_possibles_cavalier(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	if(piece.case%8 != 0 && piece.case>=16 && get_proprietaire_piece_case(piece.case-17)!=piece.joueurB):
		casesPossibles.append([piece,piece.case-17])
	if(piece.case%8 != 7 && piece.case>=16 && get_proprietaire_piece_case(piece.case-15)!=piece.joueurB):
		casesPossibles.append([piece,piece.case-15])
	if(piece.case%8 > 1 && piece.case>=8 && get_proprietaire_piece_case(piece.case-10)!=piece.joueurB):
		casesPossibles.append([piece,piece.case-10])
	if(piece.case%8 < 6 && piece.case>=8 && get_proprietaire_piece_case(piece.case-6)!=piece.joueurB):
		casesPossibles.append([piece,piece.case-6])
	if(piece.case%8 > 1 && piece.case<=55 && get_proprietaire_piece_case(piece.case+6)!=piece.joueurB):
		casesPossibles.append([piece,piece.case+6])
	if(piece.case%8 < 6 && piece.case<=55 && get_proprietaire_piece_case(piece.case+10)!=piece.joueurB):
		casesPossibles.append([piece,piece.case+10])
	if(piece.case%8 != 0 && piece.case<=47 && get_proprietaire_piece_case(piece.case+15)!=piece.joueurB):
		casesPossibles.append([piece,piece.case+15])
	if(piece.case%8 != 7 && piece.case<=47 && get_proprietaire_piece_case(piece.case+17)!=piece.joueurB):
		casesPossibles.append([piece,piece.case+17])
	return casesPossibles

func case_possibles_pion(piece:PieceModele) -> Array:
	var casesPossibles:Array = []
	var sens = -1 if piece.joueurB else 1
#Base : un en avant
	if(get_proprietaire_piece_case(piece.case + (8*sens))==null):
		casesPossibles.append([piece, piece.case + (8*sens)])
#Double mouvement de départ
		if(piece.case > (7 if sens==1 else 47) && piece.case < (16 if sens==1 else 56)
		 && get_proprietaire_piece_case(piece.case + (16*sens))==null): 
			casesPossibles.append([piece, piece.case + (16*sens)])
#prises
	var directionsPrise:Array = []
	if(piece.case%8 != 0):
		directionsPrise.append(7 if sens==1 else 9)
	if(piece.case%8 != 7):
		directionsPrise.append(7 if sens==-1 else 9)
	for direction in directionsPrise :
		if(get_proprietaire_piece_case(piece.case + (direction*sens)) == (!piece.joueurB)):
			casesPossibles.append([piece, piece.case + (direction*sens)])
		else :
# warning-ignore:narrowing_conversion
			if(verifier_en_passant(piece.case + (sign(sens) if direction==9 else - sign(sens)), piece)):
				casesPossibles.append([piece, piece.case + direction*sens])
	return casesPossibles

func verifier_en_passant(casePrise:int, piece:PieceModele) -> bool:
	if(typeof(cases[casePrise]) != TYPE_INT):
		var cible:PieceModele = cases[casePrise]
		if(cible.estEnPassantAble && cible.joueurB != piece.joueurB):
			return true
	return false
