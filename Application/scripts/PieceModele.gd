class_name PieceModele

var piece:int
var joueurB:bool
var case:int
var estEnPassantAble:bool
var toString:String

func init(pieceN,joueurBN,caseN):
	self.piece = pieceN
	self.joueurB = joueurBN
	self.case = caseN
	estEnPassantAble = false
	init_toString()


func init_toString():
	match piece:
		0:
			toString += ("R")
		1:
			toString += ("D")
		2:
			toString += ("T")
		3:
			toString += ("F")
		4:
			toString += ("C")
		5:
			toString += ("P")
	toString += ("b" if joueurB else "n")
