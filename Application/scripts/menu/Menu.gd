extends Node2D

func _on_BtnLancer_pressed():
	globals.joueurs = [$JoueurBlanc.strategie, $JoueurNoir.strategie]
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Main.tscn")

func _on_BtnLancerRandom_pressed():
	randomize()
	globals.joueurs = [1,1]
	globals.joueurs[randi()%2] = 2
# warning-ignore:return_value_discarded
	get_tree().change_scene("res://scenes/Main.tscn")
