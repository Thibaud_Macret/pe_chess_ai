extends Node2D

class_name Piece

enum PIECES{
	ROI,#0
	DAME,#1
	TOUR,#2
	FOU,#3
	CAVALIER,#4
	PION#5
}

export (PIECES) var piece
export var joueurB:bool
# warning-ignore:unused_class_variable
export var case:int
# warning-ignore:unused_class_variable
var estEnPassantAble:bool = false
# warning-ignore:unused_class_variable
export var indexModele:int

func _ready():
	actualise_image()

func actualise_image():
	$Sprite.frame = piece + int(joueurB)*6

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_Piece_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT && globals.visuActive:
		$"../..".on_piece_click(self)
