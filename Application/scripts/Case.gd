extends Area2D

class_name Case

var id:int
var couleurBase:Color
var couleurActif:Color

# warning-ignore:shadowed_variable
func init(id:int):
	self.id = id

func _ready():
	self.id = self.get_position_in_parent()
# warning-ignore:integer_division
	couleurBase = (Color("5abc73") if (id/8%2 == id%2) else Color("278a0f"))
# warning-ignore:integer_division
	couleurActif = (Color("bcaa5a") if (id/8%2 == id%2) else Color("be9e0f"))
	reset_couleur()

func reset_couleur():
	$ColorRect.color = couleurBase

func active_case():
	$ColorRect.color = couleurActif

func active_case_choix():
	$ColorRect.color = ("B31E00")

func active_case_echec():
	$ColorRect.color = ("9f6ed1")

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_Case_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton and event.pressed and event.button_index == BUTTON_LEFT && globals.visuActive:
		if($ColorRect.color == couleurActif):
			$"../..".on_case_click(self)
