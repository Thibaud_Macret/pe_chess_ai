class_name Joueur

var joueurB:bool
var peutRoque:bool
var strategie:Strategie
var humain:bool

# warning-ignore:shadowed_variable
# warning-ignore:shadowed_variable
func init(joueurB:bool, strategie:Strategie, estHumain:bool):
	self.joueurB = joueurB
	self.strategie = strategie
	self.humain = estHumain
	self.peutRoque = true
	self.strategie.joueurB = joueurB

func jouer():
	self.strategie.jouer()

func promotion():
	return (self.strategie.promotion())
