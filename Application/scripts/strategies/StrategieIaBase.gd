extends "res://scripts/strategies/Strategie.gd"

class_name StrategieIaBase

const valeurPieces:Array = [1000,10,5,3,3,1]
const profondeurRecherche:int = 2
var nbPosEvaluees:int = 0 #AFFICHAGE
var nbTranspositionsCoupees:int = 0 #AFFICHAGE
var nbABCuts:int = 0 #AFFICHAGE
var meilleurCoup:Array
var transpositions:Array = []

func jouer():
	#Init
	var positionActuelle:String = globals.refModele.conversion_string()
	init_transpositions()
	#Traitement
# warning-ignore:return_value_discarded
	print(String(evaluer_profondeur_n_negamax(positionActuelle)) + " de score")
	print(String(nbPosEvaluees) + " positions évaluées")
	nbPosEvaluees = 0
	print("Environ " + String(nbTranspositionsCoupees) + " positions coupées par transpositions")
	nbTranspositionsCoupees = 0
	print("Environ " + String(nbABCuts) + " positions coupées par l'algo alpha/beta")
	nbABCuts = 0
	#Fin et retour
	globals.refModele.recuperation_string(positionActuelle)
	globals.refModele.bouge_piece(meilleurCoup)
	globals.refModele.joueurActif = self.joueurB
	globals.refModele.passe_tour()

func init_transpositions():
	transpositions.clear()
# warning-ignore:unused_variable
	for indexT in range(profondeurRecherche+1):
		transpositions.append([])

#Renvoie la valeur de la position en fonction de possibilités dans n coups
func evaluer_profondeur_n_negamax(positionAEvaluer:String, profondeur:int=profondeurRecherche, alpha:float=-INF, beta:float=INF, joueurActif:bool=self.joueurB) ->float:
	nbPosEvaluees += 1 #AFFICHAGE
	#Transposition
	if(transpositions[profondeur].has(positionAEvaluer)):
		nbTranspositionsCoupees += profondeur*30+1 #AFFICHAGE
		return -INF
	transpositions[profondeur].append(positionAEvaluer)
	
	#Fin de l'arbre = évaluation
	if(profondeur == 0):
		return evaluer_position(positionAEvaluer) if profondeurRecherche%2==0 else -evaluer_position(positionAEvaluer)

	#Sinon on descends avec une procédure AlphaBeta Negamax
	var estMat:bool = true
	globals.refModele.recuperation_string(positionAEvaluer)
	for coup in globals.refModele.calcule_possibilitees(joueurActif, true):
		globals.refModele.recuperation_string(positionAEvaluer)
		globals.refModele.bouge_piece(coup)
# warning-ignore:narrowing_conversion
		var score:int =  -evaluer_profondeur_n_negamax(globals.refModele.conversion_string(), profondeur-1, -beta, -alpha, !joueurActif)
		if(score >= beta): #Si score pire que les possibilitées actuelles : on coupe
			nbABCuts += profondeur*30+1 #AFFICHAGE
			return beta
		if (score > alpha) : #Si meilleur score : on actualise
			alpha =  score
			if(profondeur == profondeurRecherche): #Si on est au début de l'arbre : on note le coup comme étant meilleur
				meilleurCoup = coup
		estMat = false
	if(estMat): #Index = 0 équivaut à un échec et mat/pat
		return -INF if self.joueurB == joueurActif else INF
	return alpha 

#Renvoie la valeur d'une position
func evaluer_position(position:String) ->float:
	var valeur:float = 0
	globals.refModele.recuperation_string(position)
	valeur += compte_pieces(0 if self.joueurB else 1) - compte_pieces(1 if self.joueurB else 0)
	return valeur

func compte_pieces(indexPieces:int):
	var total:int = 0
	for piece in globals.refModele.pieces[indexPieces] :
		total += (valeurPieces[piece.piece] if piece.case != -1 else 0)
	return total

func promotion() ->int:
	return (1) #TODO


func toString()->String:
	return "IA basique, profondeur " + String(profondeurRecherche)
