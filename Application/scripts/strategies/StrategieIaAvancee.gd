	extends "res://scripts/strategies/Strategie.gd"

class_name StrategieIaAvancee

const valeurPieces:Array = [1000,10,5,3,3,1]
const nombreEvaluationsSeconde = 800
const nbSecondes:float = .01
const profondeurMinimale:int = 2

var profondeurRecherche:int = 2
var nbPosEvaluees:int = 0 #AFFICHAGE
var nbTranspositionsCoupees:int = 0 #AFFICHAGE
var nbABCuts:int = 0 #AFFICHAGE
var meilleurCoup:Array
var transpositions:Array = []

var nbTours:int = 0
var mapPieces:Array = [mapRoi1.debut, mapDame1.debut, mapTour1.debut, mapFou1.debut, mapCav1.debut, mapPion1.debut]


func jouer():
	#Init
	var positionActuelle:String = globals.refModele.conversion_string()
	init_transpositions()
# warning-ignore:return_value_discarded
	print(evaluer_profondeur_n_negamax(positionActuelle), " de score initial.")
	print(nbPosEvaluees, " positions évaluées, profondeur de ", profondeurRecherche)
	gerer_vitesse_variable(nbPosEvaluees)
	nbPosEvaluees = 0
	print("Environ ", nbTranspositionsCoupees, " positions coupées par transpositions.")
	nbTranspositionsCoupees = 0
	print("Environ ", nbABCuts, " positions coupées par l'algo alpha/beta.")
	nbABCuts = 0
	#Fin et retour
	globals.refModele.recuperation_string(positionActuelle)
	globals.refModele.joueurActif = self.joueurB
	globals.refModele.bouge_piece(meilleurCoup)
	print(evaluer_position(globals.refModele.conversion_string()), " de score après mouvement.")
	gerer_tours()
	globals.refModele.passe_tour()

func init_transpositions():
	transpositions.clear()
# warning-ignore:unused_variable
	for indexT in range(profondeurRecherche+1):
		transpositions.append([])

#Renvoie la valeur de la position en fonction de possibilités dans n coups
func evaluer_profondeur_n_negamax(positionAEvaluer:String, profondeur:int=profondeurRecherche, 
alpha:float=-INF, beta:float=INF, joueurActif:bool=self.joueurB) ->float:
	nbPosEvaluees += 1 #AFFICHAGE
	if(test_transposition(positionAEvaluer, profondeur)): #Si la position est une transposée, on coupe
		return -INF
	if(profondeur == 0): # p=0 veut dire qu'on est au bas de l'arbre, on évalue donc la pos.
		return evaluer_position(positionAEvaluer) if profondeurRecherche%2==0 else -evaluer_position(positionAEvaluer)
	#Sinon on descends d'un p avec une procédure AlphaBeta Negamax
	var estMat:bool = true
	globals.refModele.recuperation_string(positionAEvaluer) #On s'assure d'être sur ma position à évaluer
	for coup in trier_coups(globals.refModele.calcule_possibilitees(joueurActif, true)):
		globals.refModele.recuperation_string(positionAEvaluer)
		globals.refModele.bouge_piece(coup)
		var score:float =  -evaluer_profondeur_n_negamax(globals.refModele.conversion_string(), profondeur-1, 
		-beta, -alpha, !joueurActif)
		if(score >= beta): #Si score pire que les possibilitées actuelles : on coupe
			nbABCuts += profondeur*30+1 #AFFICHAGE
			if(profondeur == profondeurRecherche): #Si on est au début de l'arbre : on note le coup comme étant meilleur
				meilleurCoup = coup
			return beta
		if (score > alpha) : #Si meilleur score : on actualise
			alpha =  score
			if(profondeur == profondeurRecherche): #Si on est au début de l'arbre : on note le coup comme étant meilleur
				meilleurCoup = coup
		estMat = false #Si un coup est évalué, alors le joueur n'est pas mat
	if(estMat):
		return -INF if globals.refModele.est_en_echec(joueurActif) else 0 #Différentiation entre mat et pat
	return alpha #On renvoie le meilleur score 

func test_transposition(positionAEvaluer:String, profondeur:int) -> bool:
	if(transpositions[profondeur].has(positionAEvaluer)):
		nbTranspositionsCoupees += profondeur*30+1 #AFFICHAGE
		return true #Coupure car position déjà testée à cette profondeur
	elif(profondeur > 2): #Seconde coupure si la profondeur est grande
		if(transpositions[profondeur-2].has(positionAEvaluer)):
			nbTranspositionsCoupees += profondeur*30+1 #AFFICHAGE
			return true #Coupure car position déjà testée à p-2
	transpositions[profondeur].append(positionAEvaluer)
	return false

func trier_coups(coups:Array) -> Array:
	var coupsTries:Array = []
	var coupsParPrises:Array = [[],[],[],[],[]] 
	#Place les coups s'ils prennent rien, un pion, une pièce à 3, une tour ou une dame
	for coup in coups:
		if(typeof(globals.refModele.cases[coup[1]]) == TYPE_INT):
			coupsParPrises[-1].push_back(coup)
		else :
			match(globals.refModele.cases[coup[1]].piece):
				5:
					coupsParPrises[-2].push_back(coup)
				4:
					coupsParPrises[-3].push_back(coup)
				3:
					coupsParPrises[-3].push_back(coup)
				2:
					coupsParPrises[-4].push_back(coup)
				1:
					coupsParPrises[-5].push_back(coup)
	for partieDesCoups in coupsParPrises:
		for coup in partieDesCoups:
			coupsTries.push_back(coup)
	return coupsTries

#Renvoie la valeur d'une position
func evaluer_position(position:String) ->float:
	var valeur:float = 0
	globals.refModele.recuperation_string(position)
	valeur += compte_pieces(0 if self.joueurB else 1)*100
	valeur -= compte_pieces(1 if self.joueurB else 0)*100
	valeur += (positionnement_pieces(0 if self.joueurB else 1) - positionnement_pieces(1 if self.joueurB else 0))*5
	valeur += 150 if globals.refModele.est_en_echec(!self.joueurB) else 0
	#Retrait des positions identiques
	if(position == globals.refModele.precedentesPositions[5]):
		valeur = -INF
	if(globals.refModele.precedentesPositions.has(position)):
		valeur /= 2
	return valeur

func compte_pieces(indexPieces:int) ->int:
	var total:int = 0
	for piece in globals.refModele.pieces[indexPieces] :
		total += (valeurPieces[piece.piece] if piece.case != -1 else 0)
	return total

func positionnement_pieces(indexPieces:int) ->int:
	var total:int = 0
	for piece in globals.refModele.pieces[indexPieces] :
		if(piece.case != -1):
			total += mapPieces[piece.piece][0 if piece.joueurB else 1][piece.case]
	return total

func promotion() ->int:
	return (1) #TODO

func gerer_vitesse_variable(nbPos:int) :
	if(nbPos > nombreEvaluationsSeconde * nbSecondes): #trop lent
		profondeurRecherche = max(profondeurRecherche-1, profondeurMinimale)
# warning-ignore:integer_division
	elif (nbPos < nombreEvaluationsSeconde * nbSecondes * 50): #trop rapide
		profondeurRecherche += 1

func gerer_tours():
	nbTours += 1
	if(nbTours == 15):
		mapPieces = [mapRoi1.millieu, mapDame1.millieu, mapTour1.millieu, mapFou1.millieu, mapCav1.millieu, mapPion1.millieu]
	if(nbTours == 30):
		mapPieces = [mapRoi1.fin, mapDame1.fin, mapTour1.fin, mapFou1.fin, mapCav1.fin, mapPion1.fin]

func toString()->String:
	return "IA à vitesse varible avancée"
